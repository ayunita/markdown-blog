import React from "react"
import { Link } from "gatsby"
import "./layout.scss"

const Layout = ({ children }) => (
  <div className="wrapper">
    <div className="page-container">
      <div className="m-2 page-header">
        <Link
          to="/"
          className="link-default"
          style={{ outline: `none` }}
        >
          <h1 className="page-header-title">
            <span role="img" aria-label="mango">
            🧁
            </span>{" "}
            Cupcake Ipsum
          </h1>
          <span className="page-header-subtitle">An example of Gatsby markdown blog.</span>
        </Link>
        <div className="page-header-nav"></div>
      </div>
      <div className="page-content">{children}</div>
      <div className="page-footer">
        <p>
          Built with{" "}
          <span role="img" aria-label="cupcake">
          🧁
          </span>{" "}
          in Canada | Visit me <a href="https://ayunita.xyz/" rel="noreferrer noopener">here</a>
        </p>
        <div className="footer-sitemap">
          <Link to="/" className="link-button">
            Home
          </Link>
          <Link to="/post/about" className="link-button">
            About
          </Link>
          <Link to="/rss.xml" className="link-button">
            RSS
          </Link>
        </div>
      </div>
    </div>
  </div>
)

export default Layout
