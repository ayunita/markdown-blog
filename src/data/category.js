export const CATEGORY_LIST = [
  {
    text: "General",
    emoji: "📙",
    active: true,
  },
  {
    text: "Robot",
    emoji: "🤖",
    active: true,
  },
  {
    text: "Cupcake",
    emoji: "🧁",
    active: true,
  },
  {
    text: "Lollipop",
    emoji: "🍭",
    active: true,
  },
  {
    text: "Cake",
    emoji: "🍰",
    active: true,
  }
]
