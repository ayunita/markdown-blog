import React from "react"
import styled from "styled-components"
import Pages from "./pages"
import { Link } from "gatsby"
import "./post-link.scss"

const Image = styled.div`
  border-top-right-radius: 8px;
  border-top-left-radius: 8px;

  height: 100px;
  background: url("${props => props.imgUrl}");
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  background-color: black;
`

const PostLink = ({ title, count, pages }) => {
  const lastUpdated = pages[0].date
  const lastPostslug = pages[0].slug
  const imgUrl = pages[0].image
  
  return (
    <div className="post-link-container card">
      <div className="post-header">
        {
          imgUrl ? <Image imgUrl={imgUrl} alt={`${title} Image`}/> : null
        }
        <h2 className="post-title">
          <Link
            to={lastPostslug}
            className="link-button"
            style={{ padding: `0` }}
          >
            {title}
          </Link>
        </h2>
        <div className="post-date">Last Updated: {lastUpdated}</div>
        <div className="post-count">
          {count} {count > 1 ? "notes" : "note"} archived
        </div>
      </div>
      <div className="post-body">
        
        <Pages pages={pages} />
      </div>
    </div>
  )
}

export default PostLink
