import React, { useState } from "react"
import { graphql } from "gatsby"
import PostLink from "../components/post-link"
import CategoryBar from "../components/category-bar"
import Layout from "../layouts/layout"
import SEO from "../components/seo"
import { CATEGORY_LIST } from "../data/category"

const IndexPage = ({
  data: {
    allMarkdownRemark: { postByTitle },
  },
}) => {
  const [category, setCategory] = useState("Show All")

  const entries = [...postByTitle]
  const PostsByTitle = entries
    .sort(
      (a, b) =>
        +Date.parse(b.edges[0].node.frontmatter.date) -
        Date.parse(a.edges[0].node.frontmatter.date)
    )
    .filter(a => a.edges[0].node.frontmatter.category === category || category === "Show All")
    .map((edge, index) => {
      const title = edge.fieldValue
      const pages = edge.edges.map(e => ({
        slug: e.node.frontmatter.slug,
        page: e.node.frontmatter.page,
        date: e.node.frontmatter.date,
        image: e.node.frontmatter.image,
      }))
      return (
        <PostLink
          key={index}
          title={title}
          pages={pages}
          count={edge.totalCount}
        />
      )
    })

  return (
    <Layout>
      <SEO title="Home" />
      <div style={{ width: `100%`}}>
        <CategoryBar options={CATEGORY_LIST} selected={category} onSelected={setCategory} />
        <div className="grid-view">{PostsByTitle}</div>
      </div>
    </Layout>
  )
}

export default IndexPage

export const pageQuery = graphql`
  query {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      postByTitle: group(field: frontmatter___title) {
        fieldValue
        totalCount
        edges {
          node {
            id
            excerpt(pruneLength: 250)
            frontmatter {
              date(formatString: "MMMM DD, YYYY")
              slug
              page
              title
              tags
              category
              image
            }
          }
        }
      }
    }
  }
`
