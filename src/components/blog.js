import React from "react"
import { graphql, Link } from "gatsby"
import scrollTo from "gatsby-plugin-smoothscroll"

import Pages from "./pages"
import SEO from "./seo"
import Layout from "../layouts/layout"
import Sidebar from "./sidebar"
import Share from "./share"

import "./blog.scss"

export default function Blog({ data }) {
  const { frontmatter, html } = data.post

  const pages = data.page.edges.map(e => ({
    slug: e.node.frontmatter.slug,
    page: e.node.frontmatter.page,
    date: e.node.frontmatter.date,
  }))

  const currentPage = frontmatter.page

  const currentPageIndex = pages
    .sort((a, b) => a.page - b.page)
    .findIndex(x => x.page === currentPage)
  const previousPage =
    currentPageIndex > 0 ? pages[currentPageIndex - 1].slug : ""
  const nextPage =
    currentPageIndex < pages.length - 1 ? pages[currentPageIndex + 1].slug : ""

  const Tags = frontmatter.tags.map((tag, index) => (
    <span key={index} style={{ marginRight: `8px` }}>
      #{tag}
    </span>
  ))

  return (
    <Layout>
      <SEO title={frontmatter.title} />
      <div className="blog-container" style={{ width: `100%` }}>
        <div className="blog-header">
          <h2 id="top" style={{ marginBottom: `0` }}>
            {frontmatter.title}
          </h2>
          <span style={{ fontSize: `0.8rem`, lineHeight: `0` }}>
            Category: {frontmatter.category} | Posted on: {frontmatter.date}
          </span>
          <Pages pages={pages} activePage={currentPage} />
        </div>
        <div className="blog-body">
          <div
            className="blog-content"
            dangerouslySetInnerHTML={{ __html: html }}
          />
          <div className="blog-tags">{Tags}</div>
          <div className="mobile-share-buttons">
            <Share
              url={`https://themango.space${frontmatter.slug}`}
              desc={frontmatter.title}
            />
          </div>
        </div>
        <div className="blog-nav">
          <Link to={previousPage} className={previousPage ? "" : "hidden"}>
            <span role="img" aria-label="pointing-left">
              👈
            </span>{" "}
            Previous
          </Link>
          <Link to="/">
            <span role="img" aria-label="home">
              🏠
            </span>{" "}
            Back to Home
          </Link>
          <Link to={nextPage} className={nextPage ? "" : "hidden"}>
            Next{" "}
            <span role="img" aria-label="pointing-right">
              👉
            </span>
          </Link>
        </div>
        {/* <div className="blog-footer">
          <button onClick={() => scrollTo("#top")} className="button-to-top">
            <span role="img" aria-label="top-arrow">
              🔝
            </span>
          </button>
        </div> */}
      </div>
      <Sidebar>
        <p>Cupcake ipsum dolor sit amet. Bonbon tootsie roll bonbon chocolate bar cookie gingerbread lollipop chupa chups.</p>
        <Share
          url={`/`}
          desc={frontmatter.title}
        />
        {/* <p style={{ fontSize: `.5rem`}}>* * *</p>
        <p>Like me like you do, like me like you do...</p>
          <button>
            <span role="img" aria-label="thumbs-up">
              👍 Like
            </span>
          </button>
          <button>
            <span role="img" aria-label="thumbs-up">
              📌 Bookmark
            </span>
          </button> */}
      </Sidebar>
    </Layout>
  )
}

export const pageQuery = graphql`
  query($slug: String!, $title: String!) {
    post: markdownRemark(frontmatter: { slug: { eq: $slug } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        slug
        title
        page
        tags
        category
      }
    }
    page: allMarkdownRemark(
      filter: { frontmatter: { title: { eq: $title } } }
    ) {
      edges {
        node {
          frontmatter {
            slug
            page
            date
          }
        }
      }
    }
  }
`
