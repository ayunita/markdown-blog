---
slug: "/post/robot-1"
page: "1"
date: "2077-08-30"
title: "Robot"
tags: ["robot"]
category: "Robot"
image: "https://images.unsplash.com/photo-1586458873452-7bdd7401eabd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=480&q=80"
---
# Sample Markdown

This is some basic, sample markdown.

## Second Heading

* Robot 1
* Robot 2
* Robot 3

---
1. Robot 1
2. Robot 2
3. Robot 3

> Blockquote

**bold**, *italics*, ~~strikethrough~~

```js
// code 
const robot = 'wall-e';

const print = word => {
   return `${robot} said ${word}`;
}
```

Inline code: `const robot = 'wall-e';`.

Image of robot

![Robot](https://images.unsplash.com/photo-1589254065878-42c9da997008?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=480&q=80)