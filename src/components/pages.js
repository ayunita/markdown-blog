import React from "react"
import { Link } from "gatsby"

import "./pages.scss"
const Pages = ({ pages, activePage }) => {
  const _pages = pages
    .sort((a, b) => a.page - b.page)
    .map((p, index) => (
      <Link key={index} to={p.slug} className={activePage && activePage === p.page ? "active" : ""}>
        {p.page}
      </Link>
    ))
  return (<div className="pagination">Page: { _pages }</div>)
}

export default Pages
