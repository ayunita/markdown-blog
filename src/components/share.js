import React from "react"
import {
  FacebookShareButton,
  FacebookIcon,
  RedditShareButton,
  RedditIcon,
  WhatsappShareButton,
  WhatsappIcon,
  LineShareButton,
  LineIcon
} from "react-share"

const Share = ({ url, desc }) => {
  const iconSize = 20
  const round = true

  const style = {
      marginRight: `8px`,
      outline: `none`
  }
  return (
    <div style={{ display: `flex` }}>
      <FacebookShareButton url={url} quote={desc} hashtag="#the-mango-space" style={style}>
        <FacebookIcon size={iconSize} round={round} />
      </FacebookShareButton>
      <RedditShareButton url={url} title={desc} style={style}>
        <RedditIcon size={iconSize} round={round} />
      </RedditShareButton>
      <WhatsappShareButton url={url} title={desc} separator=":: " style={style}>
        <WhatsappIcon size={iconSize} round={round} />
      </WhatsappShareButton>
      <LineShareButton url={url} title={desc} style={style}>
        <LineIcon size={iconSize} round={round} />
      </LineShareButton>
    </div>
  )
}

export default Share
