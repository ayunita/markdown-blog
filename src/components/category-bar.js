import React from "react"
import "./category-bar.scss"

const CategoryBar = ({ options, selected, onSelected }) => {
  const categories = options
    .sort((a, b) => a.text.localeCompare(b.text))
    .map(item =>
      item.active ? (
        <button
          key={item.text}
          className={`filter-item ${selected === item.text ? "active" : ""}`}
          onClick={() => onSelected(item.text)}
        >
          <span role="img" aria-label="emoji">
            {item.emoji}
          </span>{" "}
          {item.text}
        </button>
      ) : null
    )
  return (
    <div className="category-filter">
      <button
        className={`filter-item ${selected === "Show All" ? "active" : ""}`}
        onClick={() => onSelected("Show All")}
      >
        <span role="img" aria-label="emoji">
          ✨
        </span>{" "}
        Show All
      </button>
      {categories}
    </div>
  )
}

export default CategoryBar
