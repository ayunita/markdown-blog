import React from "react"
import "./sidebar.scss"

const Sidebar = ({ children }) => {
  return <div className="sidebar-wrapper">
      {children}
  </div>
}

export default Sidebar
