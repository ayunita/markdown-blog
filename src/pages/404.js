import React from "react"

import SEO from "../components/seo"
import Layout from "../layouts/layout"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <div style={{ width: `100%`, textAlign: `center`, padding: `10%` }}>
      <h1>404: Not Found</h1>
      <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
    </div>
  </Layout>
)

export default NotFoundPage
