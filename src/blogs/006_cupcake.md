---
slug: "/post/cupcake-3"
page: "3"
date: "2077-08-05"
title: "Cupcake"
tags: ["cupcake", "delicious-cupcake", "super-cupcake", "i-love-cupcake"]
category: "Cupcake"
image: "https://images.unsplash.com/photo-1519869325930-281384150729?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=480&q=80"
---
Liquorice sweet roll pastry donut toffee cheesecake biscuit. Dragée soufflé pastry. Halvah jelly bear claw sweet toffee bear claw cookie biscuit. Cookie jelly-o muffin halvah cake gingerbread lemon drops jujubes jelly beans.

<div class="video-wrapper">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Ffqe3GnNJhc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
Lollipop marzipan tart cake sweet bonbon gummies carrot cake powder. Caramels pastry gingerbread icing candy canes. Topping croissant cake sesame snaps jelly carrot cake tiramisu macaroon caramels.

Gummies caramels candy apple pie. Pudding jelly-o chupa chups tootsie roll. Danish candy chocolate bar macaroon chocolate bar icing cake gingerbread. Bear claw powder cake jelly beans pudding marshmallow.

Candy jelly-o toffee lollipop. Carrot cake candy canes jelly. Gummi bears fruitcake wafer topping tiramisu wafer muffin carrot cake. Chocolate bar jujubes cake tiramisu marzipan gingerbread cookie brownie powder.

Sweet roll croissant pudding carrot cake. Oat cake cake lollipop soufflé candy canes cupcake fruitcake toffee tiramisu. Biscuit halvah candy cupcake chocolate bar cupcake lemon drops brownie sweet roll. Cake croissant lollipop macaroon bonbon croissant.